\chapter{Models}
\label{Models}
The following overview is based on \cite{machine-learning}.

A machine learning algorithm is defined by three components: \hspace*{3pt}(1)\hspace*{3pt} a model,\hspace*{3pt} (2)\hspace*{3pt} a loss function, and \hspace*{3pt}(3)\hspace*{2pt} an optimization procedure.
\subsection*{Model}
A \emph{model} is a set of hypotheses, while a \emph{hypothesis} is a function which assigns a label value to each example. Effectively, it does the mapping from a set of examples $\mathcal{X}$ to a set of labels $\mathcal{Y}$. Thus, a hypothesis can be written as \[h:\mathcal{X} \to \mathcal{Y}.\] In this case, since the task is a binary classification, a label value is either $0$ or $1$. In case of regression, it can be any real number. We don't know which functions are adequate hypotheses, but need to decide on potential candidates. 

With that being said, a model can be defined as \[\mathcal{H} = \{h(\boldsymbol{x};\boldsymbol{\theta})\}_{\boldsymbol{\theta}}\]
where $\boldsymbol{\theta}$ declares a set of parameters. Subscript $\boldsymbol{\theta}$ means that every hypothesis $h \in \mathcal{H}$ is defined by vector $\boldsymbol{\theta}$ (combination of parameters). However, this does not mean that model $\mathcal{H}$ is finite. For example, one can choose it to be a set of affine lines, but there's infinitely many of them.
\subsection*{Loss function}
A \emph{loss function} is a measure of how much did a single example add up to a cumulative error and is used to calculate the negative consequence of an error. More formally, loss function is defined as $L:\mathcal{Y} \times \mathcal{Y} \to \mathbb{R^{+}}$ and could be in the case of binary classification, for example, calculated as \[L(y^{(i)},h(\boldsymbol{x})^{(i)}) = |h(\mathbf{x})^{(i)} - y^{(i)}|.\]
We are talking about \emph{empirical error} because it's calculated on a training set of labeled examples $\mathcal{D}$. For this binary classification task, empirical error is defined as \[E(h|\mathcal{D}) = \frac{1}{N}\sum_{i=1}^{N}L(y^{(i)},h(\boldsymbol{x})^{(i)})\]
where $\mathbf{x}$ is a vector of features from training set and $y \in \{0,1\}$ corresponding labels. Thus, training the model is essentially finding a hypothesis $h \in \mathcal{H}$ with the smallest empirical error.
\subsection*{Optimization procedure}
An \emph{optimization procedure} is a heuristic procedure of finding a hypothesis $h^{*}$ with minimal empirical error. Since a hypothesis is defined by parameters $\boldsymbol{\theta}$, it's equivalent to finding those parameters $\boldsymbol{\theta}^{*}$.
\section{Logistic Regression}
\label{section:logistic-regression}
The following theoretical overview is based on \cite{logistic-regression}.

In order to explain logistic regression, sigmoid activation function needs to be understood first.
\emph{Sigmoid} function is defined as follows:
\begin{equation}
\label{eq:sigmoid}
	\sigma(\alpha) = \frac{1}{1 + \text{exp}(-\alpha)}
\end{equation}
The graph of this function is shown in Figure \ref{fig:sigmoid}.
\begin{figure}[htb]
	\centering
	\includegraphics[width=8cm]{img/sigmoid.png}
	\caption{Sigmoid function}
	\label{fig:sigmoid}
\end{figure}
The sigmoid function has three important characteristics. Firstly, it \textit{squashes} every argument between $0$ and $1$, which is why it's sometimes referred to as a \textit{squashing} function. Secondly, it assigns value close to $0$ to examples corresponding to one class and value close to $1$ to examples corresponding to the other. Thirdly, sigmoid function is derivable, which is important for the optimization procedure:
\[\dv{\sigma(\alpha)}{\alpha} = \dv{\alpha}(1 + \text{exp}(-\alpha))^{-1} = \sigma(\alpha)(1 - \sigma(\alpha)).\]

Now, logistic regression model can be defined as follows:
\begin{equation}
\label{eq:logistic-regression}
	h(\mathbf{x};\mathbf{w}) = \sigma(\mathbf{w}^{T}\boldsymbol{\phi}(\mathbf{x})) = \frac{1}{1 + \text{exp}(-\mathbf{w}^{T}\boldsymbol{\phi}(\mathbf{x}))} = P(y = 1|\mathbf{x})
\end{equation}
where $\mathbf{w}$ is a vector of regression model parameters, previously labeled as $\boldsymbol{\theta}$. A function $\boldsymbol{\phi}$ maps from input to feature space.
In other words, logistic regression model boils down to a probability of example belonging to class with label $y = 1$.

In logistic regression, \emph{cross-entropy error} is calculated as \[E(\mathbf{w}|\mathcal{D}) = \frac{1}{N}\sum_{i=1}^{N}\left(-y^{(i)}\ \text{ln}\ h(\mathbf{x}^{(i)};\mathbf{w}) - (1-y^{(i)})\ \text{ln}(1-h(\mathbf{x}^{(i)};\mathbf{w}))\right)\] and \emph{cross-entropy loss} as \[L(y,h(\mathbf{x})) = -y\ \text{ln}\ h(\mathbf{x}) - (1-y)\ \text{ln}(1-h(\mathbf{x})).\]

The most common \emph{optimization procedure} for logistic regression is \textbf{gradient descent}. Algorithm starts from the initial vector $\mathbf{w}$, and slowly lowers along the surface of an error function $E(\mathbf{w}|\mathcal{D})$ in an opposite direction from gradient-vector in a current point $\mathbf{w}$. The procedure is repeated until gradient effectively reaches $0$ or a small enough value. This can be written as \[\mathbf{w} \leftarrow \mathbf{w} - \eta \grad{E(\mathbf{w}|\mathcal{D})}\]
where $\eta$ is a \textbf{learning rate}. It's a size of a step taken in every iteration while trying to reach global minimum of an error function. If it is too large, optimization will diverge. If it is too small, optimization procedure might converge, but too slowly. In logistic regression, there are two approaches to gradient descent -- \emph{batch} and \emph{stochastic}. The first updates weights for all the training examples and then does a step towards minimum, while the other does it for every single example from the training set. Additional explanations can be found in \cite{logistic-regression}.

Logistic regression algorithm used was the one from \emph{scikit-learn} library.\footnote{Scikit-learn is a machine learning library for Python. \url{https://scikit-learn.org/}} It takes care of all the previously mentioned steps in finding an optimal hypothesis $h \in \mathcal{H}$. What it doesn't handle is finding optimal hyper-parameters or, essentially, an optimal model. That is carried out by an external mechanism and falls, in this case, under the responsibility of a human. Here, it is done here by grid search $5$-fold cross validation. The term ``grid search'' suggests that exhaustive search over specified grid (combination) of parameters is performed. The term ``$5$-fold'' suggest that, for every hyper-parameter combination, logistic regression model will be fit and evaluated $5$ times, once for each fold. In each of those $5$ steps, a different fold will be left out of training and be used as a test set, essentially explaining where does the term cross-validation come from. Consequently, a dataset need not be split in train, validation, and test subsets. Instead, it's only split in train and test, leaving more data to train the model on. The split ratio used is $80/20$ in favour of train data.

Before training, normalization is performed. After it, both train and test datasets have mean $\mu = 0$ and standard deviation $\sigma = 1$ with a, more or less, look of standard normally distributed data. Normalization is executed before other machine learning algorithms used in this thesis as well. There are many benefits of data normalization. Two of them mentioned here are easier optimization procedure and different coefficients comparison.

Package \emph{sklearn} does \emph{L2 regularization} by default. Essentially, it means adding a penalty term to the loss function. Hyper-parameter \emph{tol} is a tolerance for stopping criteria. It is a signal for optimization algorithm when to stop. If set too large, algorithm might ``converge'' too early, essentially not converging. On the other side -- if set too small -- the algorithm might never converge. Instead, it will reach \emph{max\_iter} (also a hyper-parameter) number of iterations and stop. Value for \emph{tol} used is $0.0001$. Along with that one, different values were tried, but without any improvements. Hyper-parameter \emph{C} defines the strength of regularization. Smaller values mean stronger regularization. \emph{Regularization} is a tool for preventing over-fitting. It is done by adding previously mentioned penalty term to empirical error. For this task, C values in hyper-parameter grid are $1$, $10$, $100$, $1000$, and $10000$. Possible \emph{solvers} for \textit{sklearn} Logistic Regression are ``newton-cg'', ``lbfgs'', ``sag'', ``liblinear'', and ``saga''. All of them, except ``liblinear'', were included in hyper-parameter grid. Solver is an algorithm used for optimization problem. Results will be shown and evaluated in Chapter \ref{ResultsEvaluation}.
\section{Support Vector Machine}
\label{section:svm}
The following overview is based on \cite{svm}.

Support vector machine algorithm finds an optimal \textbf{hyper-plane} for linearly separable classes. In the case of linearly non-separable classes, transformations are performed on original data in order to map it into a new space. For such actions, kernel functions are used. Taking the complexity of Bitcoin price movement prediction task into account, the task is definitely not linearly separable. In order to explain how basic support vector machine algorithm works, linear separableness will be assumed in the following overview.

SVM model can be written as 
\[h(\mathbf{x};\mathbf{w}, b) = \mathbf{w}^{T}\mathbf{x} + b\]
where $b$ is a bias. Now, ($n-1$)-dimensional hyper-plane in $n$-dimensional space can be defined as 
\[\mathbf{w}^{T}\mathbf{x} + b = 0.\]
We want the hyper-plane with maximum \emph{margin} (distance to the closest example).
Model prediction depends on the fact from which side of hyper-plane is an example located. Thus, prediction label can be calculated as \[y = \text{sgn}(h(\mathbf{x})).\]
Maximum-margin hyper-plane problem is illustrated in Figure \ref{fig:svm} for two dimensions only, for the sake of convenience.
\begin{figure}[htb]
	\centering
	\includegraphics[width=6cm]{img/300px-SVM_margin.png}
	\caption[Caption for SVM]{SVM maximum-margin hyper-plane\footnotemark}
	\label{fig:svm}
\end{figure}
\footnotetext{Source: \url{https://en.wikipedia.org/wiki/Support-vector_machine}}
With that said, \textbf{optimization problem} can be written as:
\[\underset{\mathbf{w}, b}{\text{argmax }}\frac{1}{||\mathbf{w}||}\]
which is, in this case, equal to
\[\underset{\mathbf{w}, b}{\text{argmin }}\frac{1}{2}||\mathbf{w}||^{2}.\]
Additionally, there's the following restriction:
\[y^{(i)}(\mathbf{w}^{T}\mathbf{x}^{(i)} + b) \ge 1, \qquad i = 1,\dots,N\]
which is obtained by scaling both $\mathbf{w}$ and $b$.
Additional details on how this is resolved can be found in \cite{svm}.

Actual algorithms used were SVC and LinearSVC from \emph{scikit-learn} package.\footnote{\url{https://scikit-learn.org/stable/modules/classes.html#module-sklearn.svm}} SVC stands for Support Vector Classification. Once again, there are hyper-parameters which need to be chosen. As in logistic regression, it is done by grid search 5-fold cross-validation.

Parameter \emph{C} is a regularization parameter, as previously explained in subsection \ref{section:logistic-regression}. Intuitively, a larger C value will result in the algorithm trying to fit in more training examples, consequently intricating the hyper-plane and making it more prone to over-fitting. Smaller C values result in simpler hyper-planes at the expense of leaving some training examples on the ``wrong'' side of the hyper-plane. As for which values of C were added to hyper-parameter grid, those are $1$, $10$, and $100$. For LinearSVC, value $100$ wasn't added, since fitting the hyper-plane lasts unacceptably long.

Parameter \emph{gamma} is a kernel coefficient. It defines influence reach of a single training example. Lower values result in even far away examples getting considerable weight, while larger gamma values lead to the decision boundary being dependent only on the examples close to the hyper-plane, making it more wiggly. The value used for \emph{gamma} was
\[\mathit{gamma} = \frac{1}{\text{n\_features} \cdot \text{Var}(\mathbf{X})}\]
where $\mathbf{X}$ is a matrix of size $\mathit{n\_examples \cdot n\_features}$. That is a default value. Variance is a squared standard deviation, which was previously calculated with formula (\ref{eq:standard-deviation}).

As previously explained in subsection \ref{section:logistic-regression}, \emph{tol} is tolerance for stopping criterion. Values added to hyper-parameter grid are $0.001$, $0.0001$, and $0.00001$.
\emph{Kernel} functions used were ``linear'', ``poly'', ``sigmoid'', and ``rbf''.\footnote{Addition explanation of mentioned kernel functions can be found here: \url{https://scikit-learn.org/stable/modules/svm.html#svm-kernels}}
\section{Multi-layer Perceptron}
Multi-layer perceptron is a multi-layer feed-forward \textbf{neural network}. In order to understand it, \textbf{artificial neuron} needs to be explained first. It is a mathematical model of a biological neuron. A neuron receives an input vector $\mathbf{x}$, applies weight vector $\mathbf{w}$ to it, sums it all up, passes through an activation function, and outputs it, making it available for neurons in the next layer. Neurons are organised into $\textbf{layers}$. Multi-layer perceptron has one \emph{input} layer, one or more \emph{hidden} layers, and one \emph{output} layer. Such an architecture can be seen in Figure \ref{fig:mlp}, where two hidden layers are used. Number of nodes in each layer is arbitrary. Layers are connected in a way such that neuron output from one layer is one of the inputs for each neuron in the following layer, with no feedbacks. Such procedure, along with loss computation, can be seen in Algorithm \ref{alg:mlp-with-backpropagation}, taken from \cite{Goodfellow-et-al-2016}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=10cm]{img/multilayer_perceptron.png}
	\caption[Caption for MLP]{Multi-layer perceptron architecture\footnotemark}
	\label{fig:mlp}
\end{figure}
\footnotetext{Source: \url{https://github.com/d-r-e/multilayer-perceptron}}

\begin{algorithm} [h]
	\caption{Multi-layer perceptron loss computation}
	\label{alg:mlp-with-backpropagation}
	\KwResult{Computed loss for a single input example}
	\begin{algorithmic}
		\REQUIRE Network depth (number of layers), \space $l$
		\REQUIRE $\boldsymbol{W}^{(i)},\ i \in \{1,\dots,l\}$, the weight matrices of the model
		\REQUIRE $\boldsymbol{b}^{(i)}, \ i \in \{1, \dots, l\}$, the bias parameters of the model
		\REQUIRE $\boldsymbol{x},$ an input to process
		\REQUIRE $\boldsymbol{y},$ the target output
		\REQUIRE $\boldsymbol{f}^{(i)}, \ i \in \{1, \dots, l\}$, activation functions used in each layer
		\STATE $\boldsymbol{h}^{(0)} = \boldsymbol{x}$
		\FOR{$k = 1, \dots, l$}
		\STATE $\boldsymbol{a}^{(k)} = \boldsymbol{b}^{(k)} + \boldsymbol{W}^{(k)}\boldsymbol{h}^{(k-1)}$ 
		\STATE $\boldsymbol{h}^{(k)} = \boldsymbol{f}^{(k)}(\boldsymbol{a}^{(k)})$
		\ENDFOR
		\STATE $\hat{\boldsymbol{y}} = \boldsymbol{h}^{(l)}$ 
		\STATE $\mathit{Error}$ = $L(\hat{\boldsymbol{y}}, \boldsymbol{y})$
		
	\end{algorithmic}
\end{algorithm}

\textbf{Activation functions} define how is the weighted sum transformed into an output. There are many of them, so we'll only touch on those used for Bitcoin price movement prediction task. In hidden layers, \emph{rectified linear unit activation function (ReLU)} is used. It is defined as:
\[\mathit{ReLU}(x) = \max(0,x).\]
For the output layer, \emph{sigmoid} is used as activation function. It was previously defined with formula (\ref{eq:sigmoid}) and Figure \ref{fig:sigmoid}.

Training the model is based on the \textbf{error back-propagation} algorithm. It is based on propagating a computed loss backwards, layer by layer, and updating weights in each layer. The weights are updated proportionally to their contribution to the error. The \textbf{loss function} used is \emph{BCE with logits loss}.\footnote{Class from PyTorch package: \url{https://pytorch.org/docs/stable/generated/torch.nn.BCEWithLogitsLoss.html}} It combines a sigmoid layer with Binary Cross Entropy loss:
\begin{align*}
	\mathit{BCEWithLogitsLoss}(\boldsymbol{x}, \boldsymbol{y}) &= L = \{l^{(1)},\dots,l^{(N))}\}^{T}, \notag \\
	\quad l^{(n)} & = -\boldsymbol{W}^{(n)}\ [\boldsymbol{y}^{(n)} \cdot \text{log}\ \sigma(\boldsymbol{x}^{(n)}) + (1 - \boldsymbol{y}^{(n)}) \cdot \text{log}(1 - \sigma(\boldsymbol{x}^{(n)}))]
\end{align*}
where $N$ is a batch size. \textbf{Batch size} is a number of examples, which are propagated through the network before calculating the error and adjusting the weights accordingly. In algorithm \ref{alg:mlp-with-backpropagation}, batch size is, for the sake of simplicity, considered to be $1$, which is not usually the case in training deep learning models. 
An optimization algorithm used for adjusting weights in order to minimize the error is the \textbf{stochastic gradient descent}. 

The library used to build a multi-layer perceptron model is \emph{PyTorch}.\footnote{\emph{PyTorch} is an open source machine learning library used for applications such as computer vision and natural language processing. \url{https://pytorch.org/}} A built model uses two hidden layers. Train, test, and validation data is, once again, normalized to have $\mu=0$ and $\sigma=1$, with more or less normal distribution. It is important to mention that, when normalization was not executed, stochastic gradient descent optimizer had trouble finding global minimum of a loss function and did not converge. Additionally, normalization is performed after using activation function in each layer. Train/validation/test split with ratio $0.64/0.16/0.2$ is used. The number of \textbf{epochs} used is $400$. Early stopping is also used, which means that if validation loss (loss calculated on validation set) did not reduce in neither of previous $25$ epochs, training is terminated. After training, the model with the lowest validation loss is taken as a representative.

Once again, hyper-parameters need to be defined by hand. Batch sizes of $32$, $64$, and $128$ are added to hyper-parameter grid. Learning rate values which were added are $0.01$, $0.001$, $0.0001$, and $0.00001$. For a number of neurons in both first and second hidden layer, values $128$, $256$, and $512$ were taken. As dropout values, $0.1$ and $0.2$ were added to the grid. \textbf{Dropout} is an inexpensive and powerful regularization method, whose purpose is preventing over-fitting and improving generalization. It is based on probabilistically dropping out nodes during training.