\chapter{Datasets and Preprocessing}
\label{DatasetsAndPreprocessing}
\section{Datasets}
\label{section:datasets}
All the datasets were loaded using \emph{Pandas} package.\footnote{\emph{Pandas} is a Python library for data manipulation and analysis. \url{https://pandas.pydata.org/}}
\subsection{Tweets}
Tweets were not manually collected as a part of this thesis. In fact, they were taken from Kaggle \cite{KaggleTweets}, where they were published under CC0: Public Domain Dedication.\footnote{\emph{Kaggle} is an online community of data scientists and machine learning practitioners. \url{https://www.kaggle.com}} Tweets were collected from \date{January 1, 2016} to \date{March 29, 2019}, with one part of them being collected in real-time and the other historically. For fetching historical tweets, an open-source scraping tool named Twint was used.\footnote{\emph{Twint} is an advanced Twitter scraping tool. \url{https://github.com/twintproject/twint}} Real-time tweets were gathered with Twitter API for Python -- Tweepy.\footnote{\emph{Tweepy} is a Python library for accessing Twitter API. \url{https://github.com/tweepy/tweepy}} In order for a tweet to be fetched, it needed to contain either BTC or Bitcoin -- of course -- case insensitive. Only original tweets were gathered, meaning that retweets are excluded. That way, a total number of $2,641,883$ tweets were collected. Table \ref{tbl:twitter-columns} provides us an insight on available columns for each tweet. 

\begin{table} [htb]
	\caption{Twitter dataset columns}
	\label{tbl:twitter-columns}
	\centering
	\small
	\begin{tabular}{ l l }
		\hline
		Column & Description \\
		\hline
		\hline
		Id & Unique tweet identifier \\
		User & Username of a tweet author \\
		Full Name & Name and surname of a tweet author \\
		URL & Tweet URL in form of \textit{user/status/id}. Has NA value for most tweets. \\
		Timestamp & Date and time of tweet creation. \\
		Replies & Number of tweet replies. \\
		Likes & Number of tweet likes. \\
		Retweets & Number of tweet retweets. \\
		Text & Tweet text. \\
		\hline
	\end{tabular}
\end{table}
It's important to mention that it's unknown which tweets were gathered in real-time and which historically. Dataset columns \textit{replies}, \textit{likes}, and/or \textit{retweets} could be inspected for that purpose, since they should have significantly higher values in case of historically collected tweets. Namely, real-time tweet gathering simply doesn't provide enough time for a tweet to receive replies, likes, or retweets.
Figure \ref{fig:average-number-of-tweet-replies-per-day} could indicate that tweets after \textit{roughly} May 2017 were collected historically and the ones prior to that in real-time.
\begin{figure}[htb]
	\centering
	\includegraphics[width=16cm]{img/average_number_of_tweet_replies_per_day.png}
	\caption{Average number of replies on a tweet per day}
	\label{fig:average-number-of-tweet-replies-per-day}
\end{figure}

Another crucial information would be the number of tweets per day, shown in Figure \ref{fig:number-of-tweets-per-day}. Here, a major inconsistency is to be noticed. There are large spikes in the number of tweets per day, which is presumably due to tweets gathering. Bitcoin price is subject to volatility, which could lead to a higher number of tweets on some days. However, such spikes are infeasible. The spikes are not a problem in itself, as is an insufficient number of tweets on other days. According to \cite{Paper-PricePredictionUsingTweetVolumesAndSentimentAnalysis}, there is a strong correlation between daily tweet volume and daily Bitcoin price. More precisely, they state that the calculated Pearson's $r$ is $0.841$ with a p-value of $0.000$. Pearson correlation coefficient, as well as p-value will be defined and further discussed in chapter \ref{ResultsEvaluation}. Interestingly enough, the day with most tweets was February 6, 2018. That's the day the price of Bitcoin reached $6,852$ USD, meaning a fall of $283\%$ in just $52$ days.
\begin{figure}[htb]
	\centering
	\includegraphics[width=16cm]{img/number_of_tweets_per_day.png}
	\caption{Number of tweets per day}
	\label{fig:number-of-tweets-per-day}
\end{figure}

\subsection{Bitcoin Historical Price}
Finding Bitcoin historical data is an effortless task as there are plenty of datasets covering that. The chosen one is also from Kaggle \cite{KaggleBitcoin}. There were three different intervals available in the dataset -- $1$-min, $1$-hour, and day interval. The chosen interval to work with was $1$-hour. Choosing $1$-min interval would lead to a lot of data to work with, which is desirable in machine and deep learning. Nonetheless, tweets influence couldn't be captured in such a short time period. On the other hand, choosing a day interval wouldn't provide enough data to build the models upon. Therefore, a sweet spot of an hour -- or as it will be seen in Chapter \ref{FeatureExtraction} -- a few hours, was chosen.

Bitcoin historical price was available from October 8, 2015 until April 9, 2020. However, only the rows overlapping with the Twitter dataset were chosen. The columns available in the dataset are shown in Table \ref{tbl:bitcoin-columns}.
\begin{table} [htb]
	\caption{Bitcoin Historical Dataset columns}
	\label{tbl:bitcoin-columns}
	\centering
	\small
	\begin{tabular}{ l l }
		\hline
		Column & Description \\
		\hline
		\hline
		Unix Timestamp & Number of seconds elapsed since the Unix epoch. \\
		Date & Date (and time in case of a $1$-hour interval) of data recorded. \\
		Symbol & BTC-USD for all rows. Represents Bitcoin value in US Dollars. \\
		Open & Opening price for a chosen time interval. \\
		High & The highest price in a chosen time interval. \\
		Low & The lowest price in a chosen time interval. \\
		Close & Closing price of a chosen time interval. \\
		Volume & Number of tokens (Bitcoins) traded within a certain period of time. \\
		\hline
	\end{tabular}
\end{table}
Information about values of open, close, high, and low are standards in trading. Together, these four form a candlestick which is used in combination with other candlesticks for the purpose of identifying chart patterns.\footnote{Candlestick displays the high, low, open, and closing prices for a specific period. The wide part of the candlestick tells if the closing price was higher (green) or lower (red) than the opening price. The candlestick's shadows provide information about the interval's lowest and highest price.} 

\begin{figure}[!hb]
	\centering
	\includegraphics[width=16cm]{img/btc_price_volume.png}
	\caption{Bitcoin price and trading volume}
	\label{fig:btc-price-volume}
\end{figure}

It is necessary to visualise how did the Bitcoin price, as well as its trading volume, behave during the examined period of time, which can be observed on Figure \ref{fig:btc-price-volume}. It shows daily opening price and trading volume. Volume is more volatile than price, which comes as no surprise. High volatility in volume does not necessarily mean price going neither up nor down. It just represents trading traffic for that day. A volume that's going to be used in models will be in Bitcoins, rather than USD, since it represents trading volume more prominently, owing to the fact that it's not influenced by the current price of a single Bitcoin.
It's worth to mention that a day with the lowest price ($353.32$\$) is January 16, 2016, while the one with the highest ($19,999.00$\$) is December 17, 2017.

\section{Preprocessing}
Preprocessing text before it being used to build the models is a common step in NLP. The reason behind this is to give computers an easier job of processing the text by removing unnecessary information from it or putting it in a processable form. Amount of preprocessing to be applied depends on the text dataset which is being dealt with, as well as intentions one has with that dataset. If the dataset is too noisy and it's not processed enough, it will give rise to the \textit{garbage-in-garbage-out} phenomenon. 

Since all the tweets containing either BTC or Bitcoin were gathered, there's a chance of a tweet being non-English. The first approach was to use Googletrans API.\footnote{\url{https://py-googletrans.readthedocs.io/en/latest/}} However, there's a request limit which was very quickly exceeded. As a reminder, we're dealing with $2,641,883$ tweets. Thus, an API with no request rate limit was needed. The one found was \emph{whatthelang}.\footnote{\url{https://github.com/indix/whatthelang}} A language was predicted for every single tweet and here are the results. Without preprocessing, percentage of English tweets, as recognized by \emph{whatthelang}, is $73.86\%$. When preprocessing is applied, that percentage sinks to $71.69\%$, which is an unexpected behaviour. Nevertheless, only English tweets are kept in both cases.

Furthermore, in this case, we're dealing with tweets, which have a few tweet-specific text features -- links, mentions, and emojis. An approach of both removing and not removing these from the text was taken in order to investigate the differences in those two techniques. 
Both links and mentions were completely removed from the dataset using Python \texttt{re} module. A chunk of code producing such outcome can be seen in Listing \ref{lst:preprocessing}.
\begin{Code}
	\centering
	\begin{lstlisting}[language=Python, label={lst:preprocessing}, caption={Link and mention removal}]
		tweet = re.sub("@[A-Za-z0-9_]+",
			"", tweet) # remove mentions
		tweet = re.sub(r"(?:\@|http?\://|https?\://|www)\S+",
			"", tweet) # remove links
	\end{lstlisting}
\end{Code}

When it comes to emojis, they were converted into their text representation using the \texttt{emoji} package.\footnote{\url{https://pypi.org/project/emoji/}}
\ For example, emoji \emojimoneybag would be turned into text \textit{money bag}. With that being said, Table \ref{tbl:tweet-preprocessing} shows an example of a tweet prior to and after preprocessing.\footnote{Disclaimer: This tweet does not exist and is here just for preprocessing representation purposes.}
\begin{table} [htb]
	\caption{Tweet text preprocessing}
	\label{tbl:tweet-preprocessing}
	\centering
	\small
	\begin{tabular}{ l l }
		\hline
		Before & @fer\_unizg - https://www.fer.unizg.hr/ Do you think Bitcoin has a bright future? \emojimoneybag \\
		After & - Do you think Bitcoin has a bright future? money bag \\
		\hline
	\end{tabular}
\end{table}
With such an approach, emojis are represented as text, which provides more information. Thus, those tweets are now eligible for extracting sentiment and keywords from them.

Lowercasing is another usual form of text preprocessing. However, for some tasks, we'd like to keep the casing since it does provide additional information about the feelings of a tweet author. Hence, lowercasing will be carried out for some tasks only, which will be further discussed in Chapter \ref{FeatureExtraction}.

Other popular preprocessing techniques, although usually task specific, are:
\begin{itemize}
	\item tokenization,
	\item stemming and lemmatization,
	\item stop words removal,
	\item punctuation removal,
	\item noise removal,
	\item normalization.
\end{itemize}
\emph{Tokenization} is a technique of separating text into smaller units -- tokens so that they could be examined and processed individually.
\emph{Stemming} is a process of reducing a word to its pseudo stem. It is based on a simple rule-based approach. For instance, if the word ends in ``ed'', ``ing'', or ``ly'', remove these suffixes. This is sometimes referred to as crude, since it is not the case for all parts of speech to have such a defined set of rules. \emph{Lemmatization} tries to improve upon this problem.  It tries to group together the inflected word forms in order to be analysed as a single term, usually referred to as \emph{lemma}, or dictionary form. For that reason, lemmatization is diverse and usually performs better than stemming. \emph{Stop words removal} is a simple process of removing stop words defined by a specific language. Examples of some of the English stop words would be \textit{to, I, he, is, not, too}, etc. \emph{Punctuation removal} is a similar process where all the punctuation is removed, as it -- most of the times -- does not provide any added information. \emph{Noise removal} makes sure to remove character digits and pieces of text which could potentially interfere with text analysis. For example, it would turn \textit{..3trouble!} into \textit{trouble}. \emph{Normalization} tries to transform text into standard form. It would transform \textit{2morrow} into \textit{tomorrow}, \textit{b4} into \textit{before} and \textit{:-)} into \textit{smile}. These task-specific techniques were not used in this case. It will be more clear why when feature extraction gets discussed in Chapter \ref{FeatureExtraction}.