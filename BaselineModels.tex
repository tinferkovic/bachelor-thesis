\chapter{Baseline models}
\label{BaselineModels}
An attempt to predict Bitcoin price movement with Twitter analysis only is the goal of this thesis. However, some baseline models are needed in order to see how well are those predictions performing. This is always a good approach in machine learning, so that one could see how successful are his/her predictions. There are many things which could be predicted regarding Bitcoin price -- exact opening, closing, highest, or lowest price during an examined hour. Another approach is to predict whether or not the opening, closing, lowest, or highest price during an examined hour will be higher or lower compared to the hour before. Taking into account how difficult Bitcoin price prediction is, the second approach is more reasonable. Opening and closing prices seem like a bad choice because they're just prices at the beginning and at the end of an hour, which is less relevant. Thus, a choice was made to \textbf{predict if the highest price during the examined hour will be higher or lower compared to the highest price during the previous hour}. For that purpose, a label \emph{increase\_in\_high} is introduced and assigned value $1$ if examined High is higher compared to last hour's High, $0$ otherwise.
\section{Majority Class Baseline}
\label{section:majority-class-baseline}
This is the most basic approach. Dataset is split into two, one for training, the other for testing. It is done in a $80/20$ ratio in favour of training set, which is a common split ratio in machine learning. A reason for doing so will be further explained in Chapter \ref{Models}.
After splitting the dataset, it can be seen that the majority class in the training set is $0$. This means that in the majority of examples in the training set, the highest price during previous hour was either higher or same as the highest price during examined hour.

Majority class baselines will, in this case, assign $0$ to all the predictions of label \emph{increase\_in\_high}. By doing so, accuracy of $58.09\%$ is obtained. This means that baseline model's prediction was correct $58.09\%$ of the time. Classification evaluation metrics will, however, be further defined in subsection \ref{section:performance-evaluation-metrics}.
\section{Price Indicators Baseline}
The purpose of this baseline is to try and extract as much information as possible from previous price and volume movements. So as to achieve that, seven price indicators are introduced throughout the following subsections.

\subsection*{Relative Strength Index}
The following overview is based on \cite{rsi}.

The relative strength index (RSI) is a momentum indicator which measures the magnitude of recent price changes in order to identify overbought or oversold conditions.\footnote{Momentum indicator is a technical analysis tool used to determine the strength or weakness of an asset's price.} It is an oscillator which can have a value from $0$ up to $100$. The most basic interpretation is that RSI values under $30$ represent oversold, while the ones over $70$ indicate overbought conditions. With that said, RSI (much) below $30$ could indicate a soon trend reversal to the upside, while the ones reading (much) over $70$ could be a sign of price correction and trend reversal to the downside. An intuitive explanation of this could be that if the asset is, for instance, overbought, then there could be more people trying to sell (in order to withdraw their profit) than those wanting to buy (because the asset is more expensive and you get less of it for the same amount of money).

RSI calculation is done in two steps. First, let's define average gain and average loss. It is an average percentage gain or loss during a look-back period. Positive value is used for both average gain and loss. The standard is $14$ periods to calculate $RSI_{step One}$. So, we come up with the following formula \ref{eq:rsi-step-one}.
\begin{equation}
	RSI_{\mathit{step One}} = 100 - \frac{100}{1 + \frac{\mathit{Average Gain}}{\mathit{Average Loss}}}
\label{eq:rsi-step-one}
\end{equation}
Once there are $14$ prior periods of data available, the second RSI step can be calculated using the formula that smooths out the results, as follows:
\[RSI_{\mathit{step Two}} = 100 - \frac{100}{1 + \frac{(\mathit{Previous Average Gain} \ast 13) + \mathit{Current Gain}}{(\mathit{Previous Average Loss} \ast 13) + \mathit{Current Loss}}}\]
Current gain and loss, of course, indicate gain/loss for the hour prior to the examined one, since one is not allowed to use any data from the future while processing the training set.
Along with latest RSI values for the currently examined hour, RSI values for up to ten hours prior to it are added as features. That is in attempt to capture which moment in the past when an asset was overbought or oversold provides the most information about the asset's highest price going up or down in the next hour compared to the previous one.

\subsection*{Exponential Moving Average}
\label{subsection-rma}
The following overview is based on \cite{ema}.

Exponential moving average (EMA) is a type of moving average. A purpose of a moving average is smoothing out the asset's price data by creating a constantly updated average price. Additionally, exponential moving average puts more significance on the most recent data points. In order to understand exponential moving average, simple moving average (SMA) needs to be explained first. It simply calculates the average of a selected range of prices, as follows:
\begin{equation*}
\mathit{SMA}_{n} = \frac{\mathit{Open}_{1} + \mathit{Open}_{2} + \dots + \mathit{Open}_{n}}{n}
\label{eq:sma}
\end{equation*}
where $n$ is the number of previous periods.
Now, suppose one wants to use $x$ periods as the number of observations for the EMA. Then, one must wait until the $x$-th period to obtain SMA. On the $(x+1)$-th day, one can use the SMA from the previous day as the $\mathit{EMA}_{\mathit{previous}}$.
Value of $\mathit{EMA}_{\mathit{current}}$ is calculated using the following formula:
\begin{equation*}
\mathit{EMA}_{\mathit{current}} = \mathit{Open}_{\mathit{current}} \cdot \alpha + \mathit{EMA}_{\mathit{previous}} \cdot \left(1 - \alpha \right)
\label{eq:ema}
\end{equation*}
where:
\begin{equation*} 
\alpha =  \frac{\mathit{Smoothing}}{1 + \mathit{Period}}
\end{equation*}
and $\mathit{Smoothing}$ is the smoothing factor, generally set to the default value of $2$.
Two EMA-s, with $\alpha$ values of $0.1$ and $0.3$, were chosen as features. As it can be seen from (\ref{eq:ema}), the lower the $\alpha$, the greater the smoothing and EMA is less responsive to recent changes. On the other hand, $\alpha$ values close to one result in EMA with less smoothing which gives greater weight to recent changes in price data.
\subsection*{Moving Average Convergence Divergence}
The following overview is based on \cite{macd}.

Moving average convergence divergence (MACD) is a trend-following momentum indicator which shows the relationship between two moving averages of an asset's price. These two moving averages are $26$-period exponential moving average (EMA) and $12$-period EMA, as explained previously. MACD line is calculated by subtracting the first from the second one. Signal line -- essentially a $9$-period EMA of the MACD -- is also calculated and plotted on top of MACD line in order to identify buy and sell signals. Such buy and sell signals can be seen on Figure \ref{fig:macd-signal-line}. Green dots appear when MACD line crosses signal line from below and they're represented as buy signals. On the other hand, red dots appear when MACD line crosses signal line from above and they indicate sell signals. Signal line is simply a smoothing (averaging) MACD line.
\begin{figure}[htb]
	\centering
	\includegraphics[width=16cm]{img/macd_signal_line.png}
	\caption{Buy and sell signals using MACD and signal line}
	\label{fig:macd-signal-line}
\end{figure}
\subsection*{On-Balance Volume}
The following overview is based on \cite{obv}.

On-balance volume (OBV) uses volume flow to predict changes in asset's price. It could potentially show crowd sentiment which can predict a bullish or bearish future patterns.\footnote{In trading, bullish market represents the state of the market where the prices are rising or are expected to rise. Bearish market has an opposite meaning -- prices are falling or are expected to be falling in the future.} OBV is calculated as follows:
\begin{equation*}
\label{eq:obv}
	\mathit{OBV} = \mathit{OBV}_{\mathit{prev}} + 
	\begin{cases}
		\mathit{Volume}, & \text{if}\ \mathit{Open} > \mathit{Open}_{\mathit{prev}} \\
		0, & \text{if}\ \mathit{Open} = \mathit{Open}_{\mathit{prev}} \\
		-\mathit{Volume}, & \text{if}\ \mathit{Open} < \mathit{Open}_{\mathit{prev}}
	\end{cases}
\end{equation*}
It can be seen that OBV provides a running total trading volume of an asset and shows if this volume is flowing in or out of an asset. Since OBV depends on the start date, analysts usually look at the nature of OBV movements over time, instead of its current values. OBV can also be used to track large, institutional investors, sometimes referred to as ``smart money''.
Also, OBV EMA is calculated for $41$-hour period and added to a set of features. Once again, both OBV and OBV EMA are calculated for the currently examined hour and up to 10 hours prior to it. That's $20$ new features regarding on-balance volume only.
\subsection*{Stochastic Oscillator}
The following overview is based on \cite{stochastic-oscillator}.

Stochastic oscillator is another momentum indicator comparing latest known closing price of an asset to a range of its prices over a certain period of time. That period will, in this case, be $14$, which is a usual period value. Stochastic oscillator is calculated as follows:
\begin{equation*}
\%K = \left(\frac{C - L14}{H14 - L14}\right) \ast 100
\label{eq:stochastic-oscillator}
\end{equation*}
where $C$ is the most recent closing price known. $L14$ is the lowest Low price of the previous $14$ periods. $H14$ is the highest High price of the previous $14$ periods. $\%K$ is the current value of the stochastic indicator.
$\%K$ is sometimes referred to as ``fast'' stochastic indicator. In order to calculate ``slow'' stochastic indicator $\%D$, 3-period moving average of $\%K$ needs to be calculated. Only ``slow'' stochastic indicator $\%D$ is included into the set of features.
\subsection*{Rate of Change}
The following overview is based on \cite{rate-of-change}.

Rate of change (ROC) represents the speed at which an asset's price changes over a specified period of time. It gives information about price momentum. An asset with positive ROC usually outperforms the market. On the other hand, an asset's price with low or negative ROC could soon decline in value and thus can be seen as a sell signal. Another thing to keep in mind are sharp ROC spikes, which could indicate that market as such is unsustainable and overbought, meaning this asset's price could soon go down.

The larger the period taken for ROC, the further in the past we look and compare current price to the one in that period. ROC is calculated by the following formula:
\begin{equation*}
\mathit{ROC}_{t} = \left(\frac{\mathit{Open}_{\mathit{current}} - \mathit{Open}_{t}}{\mathit{Open}_{t}}\right) \ast 100
\label{eq:roc}
\end{equation*}
where $t$ is the period (how far in the past it's being looked).
$ROC_{2}$, $ROC_{3}$, $ROC_{5}$, $ROC_{12}$, and $ROC_{25}$ are added as features in price indicators baseline model.
\subsection*{Bollinger Bands}
The following overview is based on \cite{bollinger-bands}.

Bollinger Bands\textsuperscript{\textregistered} \cite{BollingerBands} are another technical analysis tool. They are, in fact, three trend lines -- simple moving average (SMA) of an asset's price and two lines plotted two standard deviations (positively and negatively) away from SMA. Simple moving average need not be calculated on Open price directly, but can be calculated on $\mathit{Typical Price} = (\mathit{High} + \mathit{Low} + \mathit{Open}) \divisionsymbol 3$. However, such approach was not taken in this thesis when calculating Bollinger Bands. Concrete procedure for calculating upper and lower Bollinger Band is given by the following formulas:
\begin{align*}
\mathit{BOLU} = \mathit{MA}(\mathit{Open}, n) + m*\sigma[\mathit{Open}, n] \\
\mathit{BOLD} = \mathit{MA}(\mathit{Open}, n) - m*\sigma[\mathit{Open}, n]
\end{align*}
where $\mathit{BOLU}$ is the Upper Bollinger Band. Value $\mathit{BOLD}$ is the Lower Bollinger Band. Value $\mathit{MA}$ represents the moving average, $n$ the number of prior periods used, and $m$ the number of standard deviations (typically $2$).
Value $\sigma[\mathit{Open}, n]$ stands for standard deviation over last $n$ periods.
Bollinger Bands with $n$ values equal to $10$ and $20$ were added to a set of features.

The closer the prices to the upper Bollinger Band, the more overbought the market is and vice versa. Upper and lower band being close together is a sign of low volatility. On the other hand, recent high volatility is manifested in upper and lower band being far apart. Around $90\%$ of price movements happen between two bands. Any crossing above upper or below lower band is called a \emph{breakout} and is considered a major event. However, it's not a trading (buy/sell) signal.