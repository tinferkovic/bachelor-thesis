\chapter{Feature extraction}
\label{FeatureExtraction}
A machine can hardly get usable data from raw text only. That's where feature extraction comes in. An important thing to keep in mind here is that we're not predicting whether the next High will be higher or lower than the previous one depending on a single tweet, but a number of them. That is a severe restriction which eliminates some modern NLP techniques.

For example, \emph{Word2Vec} approach couldn't be taken. \emph{Word2Vec} is a technique of representing each word with $n$-dimensional vector in space. In practice, $300$ is the best choice for a number of dimensions. It uses neural networks model to learn word associations from a large corpus of text. However, in practice, we never have a large enough text corpus to properly train the model in order to put every word in space exactly where it belongs. For that reason, it's better to use models pre-trained on massive text corpora. \emph{Cosine similarity} between vectors, as calculated in formula \ref{eq:cosine-similarity}, following Euclidean dot product formula \ref{eq:euclidean-dot-product}, indicates semantic similarity of corresponding words. The more semantically similar the words are, the smaller their cosine similarity is.
\begin{equation}
\label{eq:euclidean-dot-product}
	\vec{A} \cdot \vec{B} = ||\vec{A}||\ ||\vec{B}||\ \mathit{cos}(\theta)
\end{equation}
\begin{equation}
\label{eq:cosine-similarity}
	\mathit{similarity} = \mathit{cos}(\theta) = \frac{\vec{A} \cdot \vec{B}}{||\vec{A}|| \cdot ||\vec{B}||} = \frac{\sum_{i=1}^{n}A_{i}B_{i}}{\sqrt{\sum_{i=1}^{n}A_{i}^{2}}\sqrt{\sum_{i=1}^{n}B_{i}^{2}}}
\end{equation}
It would be possible to average out all the vectors from a single tweet into one vector representing that tweet. However, in this task, we're dealing with tens, hundreds, or even thousands of tweets, whose averaging would result in too much information loss. Thus, \emph{Word2Vec} approach needed to be abandoned.
\section{Sentiment}
\emph{Sentiment} is a view or opinion that is held or expressed. By analysing it, one can find out -- from text only -- whether the person feels negative, neutral, or positive about something. That can be a great indicator of person's future intentions, including buying or selling Bitcoin. Previous work, such as \cite{PredictingBitcoinPriceFluctuationWithTwitterSentimentAnalysis}, indicates correlation between Twitter sentiment and Bitcoin price fluctuation. Elbagir et al. \cite{TwitterSentimentAnalysisUsingNaturalLanguageToolkitAndVADERSentiment} conclude that VADER Sentiment Analyser was an effective choice for sentiment analysis classification using Twitter data. Tweets they were analysing were regarding the US elections. Taking that into account, \emph{VADER} \cite{vader} was chosen as a sentiment analysis tool.\footnote{VADER is an abbreviation for Valence Aware Dictionary and sEntiment Reasoner} It is a lexicon and rule-based sentiment analysis tool which is specifically attuned to sentiments expressed in social media. 

VADER takes a tweet and returns four values -- ``pos'', ``neg'', ``neu'', and ``compound''. The \emph{compound} score is calculated as a sum of the valence scores of each word in the lexicon that is appearing in the tweet, adjusted according to the rules, and then normalized between $-1$ and $+1$. It's the most useful unidimensional measure of sentiment for a given tweet. Thus, \emph{compound} component is used as a sentiment composite score of a tweet for this task. The \emph{pos}, \emph{neg}, and \emph{neu} are ratios for proportions of text that fall in each category. These were not used.

As per its documentation, VADER takes into account casing, punctuation, emojis, smiles, and many other components.\footnote{\url{https://github.com/cjhutto/vaderSentiment}}
Table \ref{tbl:vader} shows VADER in action on some of the tweets from the dataset. It perfectly explains why punctuation wasn't removed from tweets, as well as why tweets weren't lowercased before this task.
\begin{table} [htb]
	\caption{VADER sentiment compound score examples}
	\label{tbl:vader}
	\centering
	\small
	\begin{tabular}{ p{13cm} r }
		\hline
		Tweet & Compound \\
		\hline
		\hline
		Bitcoin’s most important sector realising that it's kinda shit. & $-0.2991$ \\
		Bitcoin’s most important sector realising that it's shit. & $-0.3626$ \\
		Bitcoin’s most important sector realising that it's SHIT. & $-0.5007$ \\
		Bitcoin’s most important sector realising that it's SHIT! & $-0.5472$ \\
		Bitcoin’s most important sector realising that it's SHIT!!! & $-0.6269$ \\
		\hline
		Australian finance watchdog to monitor Bitcoin exchanges \url{http://bit.ly/2A3TGyl} \#bitcoin \#HODL \#BTC \#Invest & $0.0$ \\
		\hline
		Nothing but grace and glad to see you’re enjoying this! I’m super impressed with your calls on Twitter the last few months regarding BTC. Hands down the best. Enjoy your success! & 0.9881 \\
		\hline
	\end{tabular}
\end{table}
Next step was to calculate the arithmetic mean (formula \ref{eq:arithmetic-mean}) and standard deviation (formula \ref{eq:standard-deviation}) of those sentiment compound scores in an attempt to express current crowd feelings and intentions. Standard deviation is calculated in an effort to capture polarity within people's stances.
\begin{align}
\label{eq:arithmetic-mean}
\mu &= \frac{1}{n}\ \sum_{i=1}^{n}a_{i} \\ 
\sigma &= \sqrt{\frac{1}{n}\ \sum_{i=1}^{n}(a_{i} - \mu)^{2}}
\label{eq:standard-deviation}
\end{align}
As per \cite{PredictingBitcoinPriceFluctuationWithTwitterSentimentAnalysis}, the most effective time-frame of prior tweet collecting is $3$ hours, followed by $2$ hours. However, an approach of looking from $1$ hour up to $10$ hours prior was taken and all the results are shown in Section \ref{ResultsEvaluation}.
\section{Keywords}
Sentiment analysis can't capture everything though. There are specific crypto terms which VADER would mostly interpret as neutral. For that reason, list of crypto positive and negative keywords/terms, as well as names of exchanges was conducted, as it can be seen in table \ref{tbl-keywords}. 
\begin{table} [htb]
\centering
\small
\caption{Exchanges names, positive, and negative crypto keywords/terms\label{tbl-keywords}}
\begin{tabular} {p{4.8cm} | p{4.8cm} | p{4.8cm}}
	\hline
	Exchanges & Positive terms & Negative terms \\
	\hline
	Coinbase, Binance, Gemini, BlockFi, Kraken, Robinhood, eToro, Crypto.com, and Coinmama. & ATH (all-time high), bubble, bull, BTFD (buy the fucking dip), bear trap, decentralized, fiat, bank-controlled, FOMO (fear of missing out), gain, halving, HODL (hold on for dear life), up, high, buy, invest, sky, lambo, moon, rise, and trustless. & Ponzi, bear, bull trap, correction, dip, FUD (fear, uncertainty, and doubt), down, low, sell, rekt, short, fall, weak hands, ATL (all-time low), and drop. \\
	\hline
\end{tabular}
\end{table}

Here, the less intuitive terms will be explained. \emph{Bubble} is a term given to cryptocurrencies by people who believe that their price is far higher than their ``real'' value. \emph{Bull} is a state of market where prices are rising or are expected to be rising in the future. \emph{Bear} is the opposite. \emph{Buy the fucking dip} is referred to as an opportunity to buy an asset at a lower price, since \emph{dip} represents a price drop. It's not rarely the case that people wait for this moment if the price has been rising for quite some time. \emph{Bear trap} would be a price movement pattern which looks like a bear market (prices dropping), but it actually indicates future price movements to the upside. \emph{Fiat (currency)} is a government-issued currency which is not backed by any commodity such as gold. Crypto world mainly believes that fiat currencies have many flaws and thus invest in cryptocurrencies. \emph{Fear of missing out} is a term used by people who buy while the price is still rising (for quite some time now) because they're afraid that it will continue to rise and they'll miss out on profit. \emph{Bitcoin halving} is an event when the reward for mining a Bitcoin is cut in half. It happens after every $210,000$ mined blocks. So far, it happened three times and every time resulted in Bitcoin price increase. \emph{Hold on for dear life (HODL)} is one of the most popular crypto terms.\footnote{HODL originates from 2018 when GameKyuubi posted a drunk rant ``I AM HODLING'', describing his poor trading skills and how he'll just keep the Bitcoins from now on instead of trading. A typo soon became a popular term.} It means don't sell under any circumstance. \emph{Sky} and \emph{moon} indicate beliefs that the price will go up significantly. \emph{Lambo} is a popular metaphor for getting rich from crypto. \emph{Ponzi (scheme)} is an investing scam which generates returns for early investors with money from later ones. \emph{Weak hands} is a description for traders who lack either confidence or resources to carry their strategies out. Thus, they usually sell with losses. \emph{Bull trap} is a price movement pattern which looks like a bull market (prices increasing), but it actually indicates future price movements to the downside.

When locating those terms in tweets, regular expressions are used and casing ignored. Attention is carefully paid on all the possible forms of terms. To illustrate that, an example of code for counting the number of \emph{sell} or \emph{ATH} term occurrences can be seen in Listing \ref{lst:counting-occurrences}.
\begin{Code}
	\centering
	\begin{lstlisting}[language=Python, label={lst:counting-occurrences}, caption={Counting the number of term occurrences}]
	def count_sell(text: str):
		regex = r"\b(sell|selling|sold|sells)\b"
		return len(re.findall(regex, text, flags=re.IGNORECASE))
	def count_ath(text: str):
		regex = r"\b(ath|all[ |-]{0,1}time high)\b"
		return len(re.findall(regex, text, flags=re.IGNORECASE))
	\end{lstlisting}
\end{Code}
The number of occurrences of positive keywords/terms are summed for all the tweets within specified time frame and set as a single feature. The same procedure is followed for negative keywords/terms.
\section{Other}
\subsection*{Number of tweets}
As previously mentioned, \cite{Paper-PricePredictionUsingTweetVolumesAndSentimentAnalysis} states a high correlation between number of tweets and Bitcoin price when observed on a daily basis. Our observation time-frames will be of a length less than $24$ hours. Despite that, this correlation is expected to hold and is thus included in the set of features.
\subsection*{Punctuation}
Punctuation is another feature which could potentially reflect a style of writing. More specifically, question and exclamation marks are the ones we're most interested in. Exclamation marks are expected to indicate either hype or panic, while question marks are supposed to reflect confusion. For those reasons, an average number of question and exclamation marks (separately) per tweet in a specified time-frame is added to the set of features. Additionally, an average number of all the other punctuation is added as a feature. 