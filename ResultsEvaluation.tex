\chapter{Results evaluation}
\label{ResultsEvaluation}
\section{Performance evaluation metrics}
\label{section:performance-evaluation-metrics}
In order to evaluate the results, evaluation metrics need to be defined first. In our case of binary classification, there are four possibilities:
\begin{itemize}
	\item \emph{true negative} (TN) -- testing example's true label is $0$ and the model predicted $0$ as well;
	\item \emph{false negative} (FN) -- testing example's true label is $1$, but the model predicted $0$;
	\item \emph{true positive} (TP) -- testing example's true label is $1$ and the model predicted $1$; and
	\item \emph{false positive} (FP) -- testing example's true label is $0$, but the model predicted $1$.
\end{itemize}
First and most intuitive performance evaluation metric is \textbf{accuracy}. It is a ratio of correctly classified examples to all the examples, calculated as
\[\text{Accuracy} = \frac{TP + TN}{TP + TN + FP + FN}.\]
\textbf{Precision} is the ratio of true positives to all the positives predicted by the model. It is calculated as
\[\text{Precision} = \frac{TP}{TP + FP}.\]
Low precision is an indicator of many false positive examples.
\textbf{Recall} is the ratio of true positives to all the positives in a dataset. Thus, it is calculated as
\[\text{Recall} = \frac{TP}{TP + FN}.\]
Low recall is an indicator of many false negative examples.
Finally, \textbf{F$_{\mathbf{1}}$ score} is the harmonic mean of precision and recall:
\[F_{1} = \frac{2}{\text{recall}^{-1} + \text{precision}^{-1}} = 2 \cdot \frac{\text{precision} \cdot \text{recall}}{\text{precision} + \text{recall}}.\]
This is a great measure against imbalanced datasets. If either precision or recall is $0$, then F$_{1}$ score will also equal $0$. Consequently, a majority class baseline on an imbalanced dataset will have a high accuracy, but F$_{1}$ score will equal $0$.

\section{Results}
Now, let's discuss the obtained results. First, we need to remind ourselves of majority class baseline results mentioned in section \ref{section:majority-class-baseline}. It had an accuracy of $0.5809$, with precision, recall, and $F_{1}$ score all equal to $0$. Furthermore, Table  \ref{tbl:results-price-indicator-baseline} shows price indicator baseline results. It can be seen that the bar is set quite high, since the results are more than satisfying for a baseline model.
\begin{table} [htb]
\centering
\small
\caption{Price indicator baseline models results\label{tbl:results-price-indicator-baseline}}
\begin{tabular} {l r r r r}
	\hline
	Model & Accuracy & Precision & Recall & $F_{1}$ score \\
	\hline
	Logistic regression & $0.6921$ & $0.7042$ & $0.4645$ & $0.5598$ \\
	Support vector machine & $0.6827$ & $0.7021$ & $0.4294$ & $0.5329$ \\
	Multi-layer perceptron & $0.6764$ & $0.6655$ & $0.4778$ & $0.5563$ \\
	\hline
\end{tabular}
\end{table}

Hyperparameters used for each of the models built in subsection \ref{tbl:results-price-indicator-baseline} are presented next.
For \emph{logistic regression}, the solver used was ``lbfgs'', $C$ was equal to $10,000.0$, and tolerance $0.0001$. In the case of \emph{support vector machine}, linear kernel worked better than non-linear ones. However, training it also took much more time. Along with the linear kernel, value $10.0$ was used for parameter $C$ and tolerance was set to $0.0001$. When it comes to \emph{multi-layer perceptron}, best batch size was $32$, learning rate $0.001$, dropout $0.1$, and the number of neurons in both first and second hidden layer $128$.

Now, models using only Twitter features are discussed. We'll start off with \emph{logistic regression}. Results, granulated per hour, can be seen in Table \ref{tbl:results-twitter-logistic-regression}. Those results are disappointing. The model underperforms price indicators baseline by far, and is only slightly better than the majority class baseline. When compared to the majority class baseline, which predicted $0$ for all the labels, this model does predict some true positives. However, observing the recall values indicates that there are still far too many false negatives. Taking tweets into account $9$ hours prior to prediction -- with no preprocessing -- can be considered as the best approach, as it yielded the highest $F_{1}$ score, meaning it learned the most from the data given.
\begin{table} [htb]
	\centering
	\small
	\caption{Twitter features logistic regression model\label{tbl:results-twitter-logistic-regression}}
	\begin{tabular} {r r r r r c}
		\hline
		Hours prior & Accuracy & Precision & Recall & $F_{1}$ score & Preprocessed \\
		\hline
		$1$ & $0.5732$ & $0.4375$ & $0.0028$ & $0.0057$ & no\\
		$1$ & $\mathbf{0.5826}$ & $0.3846$ & $0.0021$ & $0.0042$ & yes\\
		
		$2$ & $0.5803$ & $0.3333$ & $0.0013$ & $0.0025$ & no\\
		$2$ & $0.5807$ & $0.4286$ & $0.0013$ & $0.0025$ & yes\\
		
		$3$ & $0.5800$ & $0.2727$ & $0.0013$ & $0.0025$ & no\\
		$3$ & $0.5809$ & $0.5000$ & $0.0008$ & $0.0017$ & yes\\
		
		$4$ & $0.5810$ & $0.5455$ & $0.0025$ & $0.0050$ & no\\
		$4$ & $0.5809$ & $0.5000$ & $0.0008$ & $0.0017$ & yes\\
		
		$5$ & $0.5810$ & $0.5555$ & $0.0021$ & $0.0042$ & no\\
		$5$ & $0.5807$ & $0.4000$ & $0.0008$ & $0.0017$ & yes\\
		
		$6$ & $0.5816$ & $\mathbf{0.7500}$ & $0.0025$ & $0.0050$ & no\\
		$6$ & $0.5803$ & $0.2857$ & $0.0008$ & $0.0017$ & yes\\
		
		$7$ & $0.5810$ & $0.5714$ & $0.0017$ & $0.0033$ & no\\
		$7$ & $0.5810$ & $0.6667$ & $0.0008$ & $0.0017$ & yes\\
		
		$8$ & $0.5807$ & $0.4667$ & $0.0029$ & $0.0058$ & no\\
		$8$ & $0.5808$ & $0.5000$ & $0.0013$ & $0.0025$ & yes\\
		
		$9$ & $0.5810$ & $0.5333$ & $\mathbf{0.0034}$ & $\mathbf{0.0067}$ & no\\
		$9$ & $0.5810$ & $0.5556$ & $0.0021$ & $0.0042$ & yes\\
		
		$10$ & $0.5809$ & $0.5000$ & $0.0021$ & $0.0042$ & no\\
		$10$ & $0.5810$ & $0.5814$ & $0.0017$ & $0.0033$ & yes\\
		\hline
	\end{tabular}
\end{table}

The focus is shifted to \emph{support vector machine} models now. Results are presented in Table \ref{tbl:results-twitter-svm}. They are to a great extent similar to those from logistic regression models. Thus, not much can be concluded regarding which of those models classifies better, when Twitter only features are taken into account. Previously, on the example of price indicator baseline, we've seen that logistic regression slightly outperforms support vector machine.
\begin{table} [htb]
	\centering
	\small
	\caption{Twitter features support vector machine model\label{tbl:results-twitter-svm}}
	\begin{tabular} {r r r r r c}
		\hline
		Hours prior & Accuracy & Precision & Recall & $F_{1}$ score & Preprocessed \\
		\hline
		$1$ & $0.5734$ & $0.4667$ & $0.0029$ & $0.0057$ & no\\
		$1$ & $\mathbf{0.5826}$ & $0.3846$ & $0.0021$ & $0.0042$ & yes\\
		
		$2$ & $0.5803$ & $0.3333$ & $0.0013$ & $0.0025$ & no\\
		$2$ & $0.5807$ & $0.4286$ & $0.0013$ & $0.0025$ & yes\\
		
		$3$ & $0.5796$ & $0.1111$ & $0.0004$ & $0.0008$ & no\\
		$3$ & $0.5809$ & $0.5000$ & $0.0008$ & $0.0017$ & yes\\
		
		$4$ & $0.5810$ & $0.5455$ & $0.0025$ & $0.0050$ & no\\
		$4$ & $0.5809$ & $0.5000$ & $0.0008$ & $0.0017$ & yes\\
		
		$5$ & $0.5810$ & $0.5555$ & $0.0021$ & $0.0042$ & no\\
		$5$ & $0.5807$ & $0.4000$ & $0.0008$ & $0.0017$ & yes\\
		
		$6$ & $0.5814$ & $\mathbf{0.7143}$ & $0.0021$ & $0.0042$ & no\\
		$6$ & $0.5805$ & $0.3333$ & $0.0008$ & $0.0017$ & yes\\
		
		$7$ & $0.5810$ & $0.5714$ & $0.0017$ & $0.0033$ & no\\
		$7$ & $0.5809$ & $0.5000$ & $0.0004$ & $0.0008$ & yes\\
		
		$8$ & $0.5807$ & $0.4667$ & $\mathbf{0.0029}$ & $\mathbf{0.0058}$ & no\\
		$8$ & $0.5809$ & $0.5000$ & $0.0013$ & $0.0025$ & yes\\
		
		$9$ & $0.5807$ & $0.4615$ & $0.0025$ & $0.0050$ & no\\
		$9$ & $0.5810$ & $0.5556$ & $0.0021$ & $0.0042$ & yes\\
		
		$10$ & $0.5807$ & $0.4444$ & $0.0017$ & $0.0033$ & no\\
		$10$ & $0.5810$ & $0.5714$ & $0.0017$ & $0.0033$ & yes\\
		\hline
	\end{tabular}
\end{table}

The last model to be evaluated and discussed is the \emph{multi-layer perceptron}.
Its model results can be seen in Table \ref{tbl:results-twitter-mlp}. Great improvement in both $F_{1}$ score and recall can be seen, along with a slight degradation in precision. That is the consequence of more positively classified examples. As a result, there are less false negatives, which leads to improvement in recall. However, there are also more false positives, which leads to degradation in precision. Nevertheless, recall improvement is much more significant, thus leading to an approximately $10$ times better $F_{1}$ score compared to logistic regression and SVM models. Preprocessing does seem to play a role here. Models with no preprocessing produced better results.
\begin{table} [htb]
	\centering
	\small
	\caption{Twitter features multi-layer perceptron model\label{tbl:results-twitter-mlp}}
	\begin{tabular} {r r r r r c}
		\hline
		Hours prior & Accuracy & Precision & Recall & $F_{1}$ score & Preprocessed \\
		\hline
		$1$ & $0.5754$ & $\mathbf{0.575}8$ & $0.0157$ & $0.0305$ & no\\
		$1$ & $0.5829$ & $0.4946$ & $0.0194$ & $0.0374$ & yes\\
		
		$2$ & $0.5787$ & $0.4559$ & $0.0260$ & $0.0492$ & no\\
		$2$ & $0.5806$ & $0.5000$ & $0.0239$ & $0.0457$ & yes\\
		
		$3$ & $\mathbf{0.5837}$ & $0.5556$ & $\mathbf{0.0336}$ & $\mathbf{0.0633}$ & no\\
		$3$ & $0.5798$ & $0.4651$ & $0.0168$ & $0.0324$ & yes\\
		
		$4$ & $0.5794$ & $0.4692$ & $0.0256$ & $0.0486$ & no\\
		$4$ & $0.5794$ & $0.4512$ & $0.0155$ & $0.0300$ & yes\\
		
		$5$ & $0.5793$ & $0.4622$ & $0.0231$ & $0.0440$ & no\\
		$5$ & $0.5817$ & $0.5177$ & $0.0306$ & $0.0579$ & yes\\
		
		$6$ & $0.5809$ & $0.5000$ & $0.0160$ & $0.0309$ & no\\
		$6$ & $0.5816$ & $0.5238$ & $0.0185$ & $0.0357$ & yes\\
		
		$7$ & $0.5823$ & $0.5556$ & $0.0170$ & $0.0326$ & no\\
		$7$ & $0.5810$ & $0.5040$ & $0.0264$ & $0.0503$ & yes\\
		
		$8$ & $0.5796$ & $0.4696$ & $0.0227$ & $0.0433$ & no\\
		$8$ & $0.5793$ & $0.4698$ & $0.0294$ & $0.0553$ & yes\\
		
		$9$ & $0.5814$ & $0.5118$ & $0.0273$ & $0.0518$ & no\\
		$9$ & $0.5802$ & $0.4231$ & $0.0046$ & $0.0091$ & yes\\
		
		$10$ & $0.5809$ & $0.5000$ & $0.0214$ & $0.0411$ & no\\
		$10$ & $0.5789$ & $0.4667$ & $0.0323$ & $0.0604$ & yes\\
		\hline
	\end{tabular}
\end{table}

Nonetheless, the obtained results for all three models are unsatisfactory. Stock and cryptocurrency price movement predictions are difficult tasks. Many tried out tackling them, with not much success. However, such bad results cannot be attributed to the difficulty of a task only. We were aware of the complexity of a task, and thus decided to do classification (predicting if the High price will go up or down) instead of regression (predicting exact price). As previously mentioned in subsection \ref{section:datasets}, Twitter dataset was inconsistent. Spikes in the number of tweets per day, as well as two different  techniques of tweets collecting, insinuate that not much information could be extracted from the dataset itself. \textit{Garbage-in-garbage-out} concept suggests that from a nonsense input, only nonsense output can be produced. Another obstacle that got in the way is the fact that we were not dealing with a single tweet and predicting something based on it only. In fact, we're dealing with number of them -- tens, hundreds, or even thousands in a defined time-frame -- and cannot easily use modern NLP techniques. Everything obtained from a single tweet needs to be summed or averaged across the defined time-frame. In that process, some information is certainly lost.

What showed up to be the best approach is looking for tweets $\mathbf{3}$ \textbf{hours prior} to price prediction and using a \textbf{multi-layer perceptron}. Such time-frame proved to be the best in \cite{PredictingBitcoinPriceFluctuationWithTwitterSentimentAnalysis} as well. The common problem with deep learning models, such as that one, is that they're not easily interpretable. The weights, unlike coefficients in logistic regression and SVM, cannot be interpreted as importance factors of the corresponding features. This is commonly referred to as a \emph{black-box} model, which is the opposite of a \emph{glass-box}. 

Feature elimination is an important part of \emph{supervised} machine learning, since it improves both efficiency (less space and/or time complexity) and performance. It can be done using several methods. Firstly, we could calculate \textbf{Pearson correlation coefficient}:
\[r = \frac{\sum(x_{i} - \overline{x})(y_{i} - \overline{y})}{\sqrt{\sum(x_{i} - \overline{x})^{2} \sum(y_{i} - \overline{y})^{2}}}.\]
Next, we would pick only those features which have $r$, when compared to label, greater than some predefined threshold.
Another approach, which could be implemented only for logistic regression and SVM, is the \textbf{recursive feature elimination}. It's the process of eliminating the least important feature (a feature with the smallest, least significant coefficient), building, and evaluating the model without it. If the performance metrics did not get worse, we continue without the eliminated feature and repeat the process. Another important measure when eliminating features is the \textbf{p-value}. It's the probability of seeing some effect under the assumption that the \emph{null hypothesis} is true. Thus, when p-value is low enough, we reject the null-hypothesis, because it's statistically very unlikely to happen. The lower the p-value for some feature, the more significant it is, because the null-hypothesis is that the mentioned feature is of no use.